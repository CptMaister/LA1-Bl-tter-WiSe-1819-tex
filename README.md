# Zu den Übungsblättern

Inhalte nicht von mir erstellt, sondern nur in LaTeX schöner gemacht.

PDFs, wie sie im WiSe 2018/19 veröffentlich wurden:   
[Hier](veroeffentlichte-Versionen)

Alle aktuellen Versionen (bisher nicht anders als bei den fertigen PDFs), jeweils Aufgabennummerierung von 1 beginnend:
+ [Blatt 2](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung02.pdf)
+ [Blatt 3](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung03.pdf)
+ [Blatt 4](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung04.pdf)
+ [Blatt 5](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung05.pdf)
+ [Blatt 6](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung06.pdf)
+ [Blatt 7](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung07.pdf)
+ [Blatt 8](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung08.pdf)
+ [Blatt 9](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung09.pdf)
+ [Blatt 10](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung10.pdf)
+ [Blatt 11](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung11.pdf)
+ [Blatt 12](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung12.pdf)
+ [Blatt 13](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung13.pdf)
+ [Blatt 14](https://cptmaister.gitlab.io/LA1-Bl-tter-WiSe-1819-tex/uebung14.pdf)
